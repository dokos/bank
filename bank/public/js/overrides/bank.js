frappe.ui.form.on('Bank', {
	onload(frm) {
		if (frappe.route_options.request && frappe.route_options.item_id && !frm.doc.bridge_item_id) {
			frappe.call({
				method: "validate_item_id",
				doc: frm.doc,
				args: {
					request: frappe.route_options.request,
					item_id: frappe.route_options.item_id
				}
			}).then(() => {
				frm.refresh()
			})
		}
	},
	refresh(frm) {
		frm.trigger("add_onboarding_steps");

		if (frm.doc.bridge_item_id) {
			frm.add_custom_button(__("Remove bank connection"),
				() => {
					frm.trigger("remove_bank")
				}, __("Synchronization")
			);
		}
	},

	add_buttons(frm) {
		if (!frm.is_new() && !frm.doc.bridge_item_id) {
			frm.add_custom_button(__("Connect with this bank"),
				() => {
					frm.trigger("add_new_bank");
				}
			);
		} else if (!frm.is_new() && !frm.doc.bridge_synchronization ) {
			frm.add_custom_button(__("Allow bank synchronization"),
				() => {
					frm.set_value("bridge_synchronization", 1);
					frm.save();
				}, __("Synchronization")
			);

			frm.trigger("add_common_buttons")
		} else if (!frm.is_new() && frm.doc.bridge_synchronization) {
			frm.add_custom_button(__("Synchronize all bank transactions"),
				() => {
					launch_synchronization(frm.doc)
				}, __("Synchronization")
			);

			frm.trigger("add_common_buttons")

			frm.add_custom_button(__("Stop bank synchronization"),
				() => {
					frm.set_value("bridge_synchronization", 0);
					frm.save();
				}, __("Synchronization")
			);
		}
	},

	add_common_buttons(frm) {
		frm.add_custom_button(__("Update bank accounts"),
			() => {
				frm.trigger("get_bank_accounts")
			}, __("Synchronization")
		);

		frm.add_custom_button(__("Update bank credentials"),
			() => {
				frm.trigger("edit_bank_credentials")
			}, __("Synchronization")
		);
	},

	add_onboarding_steps(frm) {
		frappe.call({
			method: "check_onboarding_status",
			doc: frm.doc
		}).then(r => {
			if (r.message && r.message.steps.length) {
				frm.dashboard.clear_headline();
				frm.dashboard.set_headline("<div></div>")
				$('.form-message').removeClass("form-message small blue").addClass("bank-onboarding form-onboarding frappe-card mb-2")
				$('.close-message').hide()

				let options = Object.assign(r.message, {
					"container": $('.form-onboarding'),
					"frm": frm
				})
				frappe.widget.make_widget(options)
			} else {
				frm.trigger("add_buttons");
			}
		})
	},

	add_new_bank(frm) {
		frappe.call({
			method: "bank.api.bridge.get_bridge_connect_link",
			args: {
				bank: frm.doc.name
			},
			freeze: true,
			freeze_message: __("Connecting..."),
		})
		.then(r => {
			if (r.message && r.message && r.message.redirect_url) {
				window.location=r.message.redirect_url
			} else {
				frappe.show_alert({
					message: __("Connection to bank failed.<br>Please retry later."),
					indicator: "red"
				})
			}
		})
	},

	edit_bank_credentials(frm) {
		frappe.call({
			method: "bank.api.bridge.get_bridge_edit_link",
			args: {
				bank: frm.doc.name,
				item_id: frm.doc.bridge_item_id
			},
			freeze: true,
			freeze_message: __("Connecting..."),
		})
		.then(r => {
			if (r.message && r.message.redirect_url) {
				window.location=r.message.redirect_url
			}
		})
	},

	sca_authentication(frm) {
		frappe.call({
			method: "bank.api.bridge.get_sca_authentication_link",
			args: {
				bank: frm.doc.name,
				item_id: frm.doc.bridge_item_id
			},
			freeze: true,
			freeze_message: __("Connecting..."),
		})
		.then(r => {
			if (r.message && r.message.redirect_url) {
				window.location=r.message.redirect_url
			}
		})
	},

	pro_confirmation(frm) {
		frappe.call({
			method: "bank.api.bridge.get_pro_confirmation_link",
			args: {
				bank: frm.doc.name,
				item_id: frm.doc.bridge_item_id
			},
			freeze: true,
			freeze_message: __("Connecting..."),
		})
		.then(r => {
			if (r.message && r.message.redirect_url) {
				window.location=r.message.redirect_url
			}
		})
	},

	get_bank_accounts(frm) {
		frappe.call({
			method: "bank.api.bridge.get_bank_accounts",
			args: {
				item_id: frm.doc.bridge_item_id
			},
			freeze: true,
			freeze_message: __("Fetching Accounts..."),
		}).then(r => {
			if (r.message && r.message.resources) {
				frm.events.make_bank_configuration_dialog(frm, r)
			} else if (r.message && r.message.message) {
				frappe.msgprint(r.message.message, title=r.message.type)
			}
		})
	},

	async make_bank_configuration_dialog(frm, r) {
		const dialog = new frappe.ui.Dialog({
			title: __("Configure your accounts"),
			size: "large",
			fields: await get_fields(r.message.resources),
			primary_action_label: __('Confirm'),
			primary_action: (values) => {
				frappe.xcall("bank.controllers.bank.configure_bank_accounts", {
					resources: r.message.resources,
					settings: values
				}).then(() => {
					dialog.hide()
					frappe.show_alert({
						message: __("The selected bank accounts are ready to be synchronized"),
						indicator: "green"
					})
					frm.refresh()
				})
			}
		});
	
		dialog.show();
	},

	remove_bank(frm) {
		frappe.xcall("bank.api.bridge.remove_item_id", {
			item_id: frm.doc.bridge_item_id
		})
		.then(() => {
			frappe.show_alert({
				message: __("The bank connection has been removed"),
				indicator: "green"
			})

			setTimeout(() => {
				frm.refresh();
			}, 2000)
		})
	}
});

const launch_synchronization = (doc) => {
	frappe.confirm(__("All accounts will be synchronized based on the last integration date or the initial synchronization date."), () => {
		frappe.show_alert({
			message: __("Synchronization started"),
			indicator: "green"
		})
		frappe.call({
			method: "synchronize_bank_transactions",
			doc: doc
		}).then(r => {
			if (r.message) {
				frappe.show_alert({
					message: r.message.length > 1 ?
						__("{0} transactions synchronized", [r.message.length]) :
						__("{0} transaction synchronized", [r.message.length])
					,
					indicator: "green"
				})
			}
		})
	})
}

const get_fields = async (resources) => {
	const accounts = await frappe.db.get_list("Bank Account", {
		fields: ["name", "bridge_account_id", "bridge_initial_date"]
	})
	const accounts_map = Object.assign({}, ...accounts.map((x) => ({[x.bridge_account_id]: [x.name, x.bridge_initial_date]})));
	return resources.reduce((prev, acc) => 
		prev.concat([
			{
				fieldtype: 'Section Break',
				fieldname: `${acc.id}_break`,
				label: acc.iban ? `${acc.name} (${acc.iban})` : acc.name
			},
			{
				fieldtype: 'Link',
				fieldname: acc.id,
				label: __("Bank Account"),
				options: "Bank Account",
				default: accounts_map[acc.id]&&accounts_map[acc.id][0]
			},
			{
				fieldtype: 'Date',
				fieldname: `${acc.id}_date`,
				label: __("Initial Synchronization Date"),
				default: accounts_map[acc.id]&&accounts_map[acc.id][1] || frappe.datetime.nowdate(),
				description: `
					${__("Please make sure there will be no conflict with previously imported transactions as the references may differ.")}
				`,
				reqd: 1
			},
		])
	, [])
}