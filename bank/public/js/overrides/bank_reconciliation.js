frappe.provide("erpnext.accounts");

frappe.pages['bank-reconciliation'].refresh = function(wrapper) {
	erpnext.bank_reconciliation.on('filter_change', async data => {
		if (data.name == 'BANK_ACCOUNT' && data.value) {
			const bank = await frappe.db.get_value("Bank Account", data.value, "bank")
			const item = await frappe.db.get_value("Bank", bank.message.bank, "bridge_item_id")

			if (item.message.bridge_item_id && !wrapper.getting_bank_status) {
				wrapper.getting_bank_status = true
				frappe.call({
					method: "bank.controllers.bank.get_bank_status",
					args: {
						bridge_item_id: item.message.bridge_item_id
					}
				}).then(r => {
					wrapper.getting_bank_status = false
					if (r.message != 0) {
						frappe.show_alert({
							message: __(`<a href="/app/bank/{}">Your bank account needs to be reconnected. Please follow the instructions on your bank's dashboard</a></div>`, [bank.message.bank]),
							indicator: "orange",
						}, 30)
					}
				})
			}
		}
	})
}