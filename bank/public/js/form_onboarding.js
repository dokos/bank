import OnboardingWidget from "frappe/public/js/frappe/widgets/onboarding_widget";

export default class FormOnboardingWidget extends OnboardingWidget {
	make_body() {
		super.make_body()
		this.show_step(this.steps.filter(s => s.is_active)[0]);
	}

	show_step(step) {
		if (!step) {
			return
		}

		if (step.action != "Trigger Event") {
			super.show_step(step)
		} else {
			this.active_step && this.active_step.$step.removeClass("active");

			if (step.is_complete) {
				return
			}

			step.$step.find(".step-skip").empty();
			step.$step.addClass("active");
			this.active_step = step;

			this.step_body.empty();
			this.step_footer.empty();

			if (step.action_label || step.action) {
				$(`<button class="btn btn-primary btn-sm">${__(step.action_label || step.action)}</button>`)
					.appendTo(this.step_body)
					.on('click', () => this.frm.trigger(step.event));
			}
		}
	}

	update_step_status() {
		//
	}

	set_actions() {
		// Remove skip
	}
}


$(document).ready(() => {
	frappe.widget.widget_factory = Object.assign(frappe.widget.widget_factory, {
		form_onboarding: FormOnboardingWidget,
	})
})