import frappe

def execute():
	bank_transactions = frappe.get_all("Bank Transaction", fields=["name", "status", "reference_number"])
	duplicate_entries = []
	for bank_transaction in bank_transactions:
		if len([b for b in bank_transactions if b.reference_number == bank_transaction.reference_number]) > 1:
			duplicate_entries.append(bank_transaction)

	cancellable_entries = [d for d in duplicate_entries if d.status == "Unreconciled"]

	added_entries = set()
	entries_to_cancel = []
	for cancellable_entry in cancellable_entries:
		if cancellable_entry.reference_number not in added_entries:
			entries_to_cancel.append(cancellable_entry)
			added_entries.add(cancellable_entry.reference_number)

	for entry_to_cancel in entries_to_cancel:
		try:
			frappe.get_doc("Bank Transaction", entry_to_cancel.name).cancel()
			frappe.delete_doc("Bank Transaction", entry_to_cancel.name)
		except Exception:
			continue