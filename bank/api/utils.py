
import bleach
from markupsafe import Markup

from frappe import _
from frappe.custom.doctype.custom_field.custom_field import create_custom_fields

def clean_html(text):
	"""
	https://stackoverflow.com/questions/8976683/jinja2-escape-all-html-but-img-b-etc
	https://stackoverflow.com/questions/54354764/python-bleach-inconsistent-cleaning-behaviour
	Perform clean and return a Markup object to mark the string as safe.
	This prevents Jinja from re-escaping the result.
	"""
	tags = bleach.sanitizer.ALLOWED_TAGS + \
		["p", "div", "br", "img", "span", "pre",
			"table", "td", "tr", "th", "h1", "h2", "h3"]
	attrs = {
		**bleach.sanitizer.ALLOWED_ATTRIBUTES,
		"img": ['src'],
	}
	return Markup(bleach.clean(text, tags=tags, attributes=attrs))


def check_if_custom_fields_exists():
	# Translatable strings for below navbar items
	__ = _("Bridge Item ID")
	__ = _('Bridge Synchronization Allowed')
	__ = _('Bridge Status')
	__ = _('Bridge Account ID')
	__ = _('Bridge Initial Synchronization Date')
	__ = _('Bridge Category ID')

	fields = {
		"Bank": [
			dict(
				fieldname='bridge_item_id',
				label='Bridge Item ID',
				fieldtype='Data',
				insert_after='plaid_access_token',
				hidden=1,
				print_hide=1,
				translatable=0
			),
			dict(
				fieldname='bridge_synchronization',
				label='Bridge Synchronization Allowed',
				fieldtype='Check',
				insert_after='bridge_item_id',
				hidden=1,
				print_hide=1,
				translatable=0
			),
			dict(
				fieldname='bridge_status',
				label='Bridge Status',
				fieldtype='Int',
				insert_after='bridge_synchronization',
				hidden=1,
				print_hide=1,
				translatable=0
			),
		],
		"Bank Account": [
			dict(
				fieldname='bridge_account_id',
				label='Bridge Account ID',
				fieldtype='Data',
				insert_after='last_integration_date',
				hidden=1,
				print_hide=1,
				translatable=0
			),
			dict(
				fieldname='bridge_initial_date',
				label='Bridge Initial Synchronization Date',
				fieldtype='Date',
				insert_after='bridge_synchronization',
				hidden=1,
				print_hide=1,
				translatable=0
			),
		],
		"Bank Transaction Category": [
			dict(
				fieldname='bridge_category_id',
				label='Bridge Category ID',
				fieldtype='Int',
				insert_after='parent_bank_transaction_category',
				hidden=1,
				print_hide=1,
				translatable=0
			),
		],
	}
	create_custom_fields(fields, update=False)