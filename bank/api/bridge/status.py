from frappe import _


BRIDGE_STATUS_MAP = {
	402: {
		"title": _("Your bank credentials are not correct"),
		"action": "Trigger Event",
		"action_label": _("Change my credentials"),
		"event": "edit_bank_credentials",
		"is_single": 1,
		"is_active": 1
	},
	429: {
		"title": _("An action is required within your online banking website"),
		"is_single": 1,
		"is_active": 1
	},
	430: {
		"title": _("Please login to your banking website to change your password"),
		"is_single": 1,
		"is_active": 1
	},
	1010: {
		"title": _("A Strong Customer Authentication is required"),
		"action": "Trigger Event",
		"action_label": _("Authenticate"),
		"event": "sca_authentication",
		"is_single": 1,
		"is_active": 1
	},
	1100: {
		"title": _("Your Pro account needs to be confirmed"),
		"action": "Trigger Event",
		"action_label": _("Confirm"),
		"event": "pro_confirmation",
		"is_single": 1,
		"is_active": 1
	},
	1003: {
		"title": _("Bank is connected but synchronization is temporarily not possible. Please try again later."),
		"is_single": 1,
		"is_active": 1
	}
}