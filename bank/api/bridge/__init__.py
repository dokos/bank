import frappe

from bank.api.utils import check_if_custom_fields_exists
from bank import dokos_client_connection
from bank.controllers.bank import synchronize_bank_transactions

def _post_request(url, args):
	check_if_custom_fields_exists()

	params = {'base_url': frappe.utils.get_url(), 'redirect_url': frappe.request.headers.get("Referer") if frappe.request and frappe.request.headers else None}
	params.update(args)
	connection = dokos_client_connection(url)

	return connection.post_request(params)


@frappe.whitelist()
def get_bridge_connect_link(bank):
	return _post_request("/api/method/bank_api.bridge.get_bridge_connect_link", {"bank": bank})

@frappe.whitelist()
def get_bridge_edit_link(bank, item_id):
	return _post_request("/api/method/bank_api.bridge.get_bridge_edit_link", {"bank": bank, "item_id": item_id})

@frappe.whitelist()
def get_sca_authentication_link(bank, item_id):
	return _post_request("/api/method/bank_api.bridge.get_sca_authentication_link", {"bank": bank, "item_id": item_id})

@frappe.whitelist()
def get_pro_confirmation_link(bank, item_id):
	return _post_request("/api/method/bank_api.bridge.get_pro_confirmation_link", {"bank": bank, "item_id": item_id})

@frappe.whitelist()
def remove_item_id(item_id):
	_post_request("/api/method/bank_api.bridge.delete_bridge_item_id", {"item_id": item_id})

	bank = frappe.db.get_value("Bank", dict(bridge_item_id=item_id))
	for account in frappe.get_all("Bank Account", filters=dict(bank=bank), pluck="name"):
		frappe.db.set_value("Bank Account", account, "bridge_account_id", None, update_modified=False)
	frappe.db.set_value("Bank", bank, "bridge_item_id", None, update_modified=False)

@frappe.whitelist()
def get_bank_accounts(item_id):
	return _post_request("/api/method/bank_api.bridge.get_bank_accounts", {"item_id": item_id})


@frappe.whitelist(allow_guest=True)
def webhook(*args, **kwargs):
	params = frappe.form_dict

	if params.webhook_type == "validate_item":
		validate(params)
	elif params.webhook_type in ("item.refreshed", "item.account.updated"):
		if not params.get("item_id"):
			return

		bank = frappe.db.get_value("Bank", dict(bridge_item_id=params.get("item_id")))
		if bank:
			synchronize_bank_transactions(bank)

def validate(params):
	if params.request:
		response = _post_request("/api/method/bank_api.bridge.get_bridge_item_id", {'request': params.request})

		item_id = response.get("item_id")
		bank_name = response.get("bank")

		bank = frappe.get_doc("Bank", bank_name)
		bank.bridge_item_id = item_id
		bank.flags.ignore_permissions = True
		bank.flags.ignore_custom_fields_creation = True
		bank.save()
		frappe.db.commit()
