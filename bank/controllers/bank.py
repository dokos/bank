import frappe
from frappe import _
from frappe.utils import flt, cstr, getdate, format_date, add_days, nowdate
from frappe.utils.nestedset import get_root_of
from frappe.utils.background_jobs import get_jobs

from erpnext.accounts.doctype.bank.bank import Bank

from bank import dokos_client_connection
from bank.api.utils import check_if_custom_fields_exists


class DokosBank(Bank):
	def validate(self):
		if not self.flags.ignore_custom_fields_creation:
			check_if_custom_fields_exists()

		self.get_bridge_item_details()

	def on_update(self):
		if hasattr(Bank, "on_update"):
			super(DokosBank, self).on_update()

	@frappe.whitelist()
	def synchronize_bank_transactions(self):
		if not self.bridge_synchronization:
			frappe.throw(_("Please enable synchronization for this bank"))

		return synchronize_bank_transactions(self.name)

	@frappe.whitelist()
	def validate_item_id(self, item_id, request):
		if self.bridge_item_id:
			return

		params = {"request": request}
		connection = dokos_client_connection("/api/method/bank_api.bridge.get_bridge_item_id")
		response = connection.get_request(params)

		if response.get("item_id") == item_id:
			self.db_set("bridge_item_id", item_id, update_modified=False)

	def get_bridge_item_details(self):
		if self.get("bridge_item_id"):
			params = {'item_id': self.bridge_item_id}
			connection = dokos_client_connection("/api/method/bank_api.bridge.get_bridge_item_details")
			response = connection.post_request(params)
			if response:
				self.bridge_status = response.get("status_code")
			else:
				self.bridge_status = 0
		else:
			self.bridge_status = 0

		return self.bridge_status

	@frappe.whitelist()
	def check_onboarding_status(self):
		self.get_bridge_item_details()
		return check_onboarding_status(self.as_dict())

	def create_notification(self):
		from bank.api.bridge.status import BRIDGE_STATUS_MAP
		try:
			subject = BRIDGE_STATUS_MAP.get(self.bridge_status)
			if not frappe.get_all("Notification Log", filters={
				"creation": ("<=", add_days(nowdate(), -10)),
				"document_type": self.doctype,
				"document_name": self.name,
				"subject": subject
			}):
				notification = frappe.new_doc("Notification Log")
				notification.set("type", "Alert")
				notification.document_type = self.doctype
				notification.document_name = self.name
				notification.subject = subject
				notification.insert()
		except Exception:
			self.log_error("Failed to send reminder")

@frappe.whitelist()
def configure_bank_accounts(resources, settings):
	settings = frappe.parse_json(settings)
	for account in settings:
		if account.endswith("_date"):
			continue

		already_connected = frappe.db.get_value("Bank Account", dict(bridge_account_id=account))
		if already_connected and already_connected != settings.get(account):
			frappe.db.set_value("Bank Account", already_connected, "bridge_account_id", None)

		frappe.db.set_value("Bank Account", settings.get(account), "bridge_account_id", account)

		if settings.get(f"{account}_date"):
			frappe.db.set_value("Bank Account", settings.get(account), "bridge_initial_date", settings.get(f"{account}_date"))

	resources = frappe.parse_json(resources)
	for resource in resources:
		if resource.get("iban"):
			connected_account = frappe.db.get_value("Bank Account", dict(bridge_account_id=resource.get("id")))
			frappe.db.set_value("Bank Account", connected_account, "iban", resource.get("iban"))

def synchronize_bank_transactions(bank, since=None):
	try:
		enqueued_method = "bank.controllers.bank._synchronize_bank_transactions"
		jobs = get_jobs()
		if not jobs or enqueued_method not in jobs[frappe.local.site]:
			frappe.enqueue(enqueued_method, bank=bank, since=since)
	except Exception:
		frappe.logger().error("******************Dokos Bridge Error******************")
		frappe.logger().error(frappe.get_traceback())
		frappe.logger().error("***************************************************")

def _synchronize_bank_transactions(bank, since=None):
	bank_accounts = frappe.get_all("Bank Account", filters={"bank": bank}, fields=["bridge_account_id", "bridge_initial_date", "last_integration_date"])
	if not bank_accounts:
		return

	if not since:
		since = min([getdate(a.last_integration_date or a.bridge_initial_date) for a in bank_accounts])

	params = {"since": format_date(since, "yyyy-mm-dd")}
	connection = dokos_client_connection("/api/method/bank_api.bridge.get_bank_transactions")
	transactions = connection.post_request(params)

	if not transactions:
		return

	authorized_accounts = {
		a.bridge_account_id: max(getdate(a.bridge_initial_date), getdate(a.last_integration_date or a.bridge_initial_date)) for a in bank_accounts
	}

	bank_transactions = transactions.get("transactions", {}).get("resources")
	categories = transactions.get("categories", {}).get("resources")

	filtered_transactions = [
		transaction for transaction in (bank_transactions or [])
		if (
			cstr(transaction.get("account_id")) in authorized_accounts.keys()
			and getdate(transaction.get("updated_at")) >= getdate(authorized_accounts.get(cstr(transaction.get("account_id"))))
		)
	]
	filtered_transactions = deduplicate_transactions(filtered_transactions)

	if categories:
		sync_categories(categories)

	if filtered_transactions:
		# Only pass transactions linked to accounts depending from this bank
		sync_transactions(filtered_transactions)

		for account, dummy in authorized_accounts.items():
			account_name = frappe.db.get_value("Bank Account", dict(bridge_account_id=account))
			frappe.db.set_value("Bank Account", account_name, "last_integration_date", getdate())

	return filtered_transactions

def deduplicate_transactions(filtered_transactions):
	added_transactions = set()
	deduplicated_transactions = []
	for filtered_transaction in filtered_transactions:
		if filtered_transaction.get("id") not in added_transactions:
			deduplicated_transactions.append(filtered_transaction)
			added_transactions.add(filtered_transaction.get("id"))

	return deduplicated_transactions

def sync_categories(categories):
	existing_categories = frappe.get_all("Bank Transaction Category", pluck="bridge_category_id")
	if not existing_categories:
		add_root_category()

	for category in categories:
		parent = category.get("id")
		if parent not in existing_categories:
			create_category(category)

		for sub_category in category.get("categories"):
			if sub_category.get("id") not in existing_categories:
				create_category(sub_category, parent)

def add_root_category():
	frappe.get_doc({
		'doctype': 'Bank Transaction Category',
		'category': _('All Categories'),
		'is_group': 1,
		'name': _('All Categories'),
		'parent_bank_transaction_category': ''
	}).insert(ignore_if_duplicate=True, ignore_permissions=True)
	frappe.db.commit()

def create_category(category, parent=None):
	parent_category = (
		frappe.db.get_value("Bank Transaction Category", dict(bridge_category_id=parent))
		if parent else get_root_of("Bank Transaction Category")
	)

	category_name = category.get("name")
	if frappe.db.exists("Bank Transaction Category", category_name):
		category_name = f"{parent_category} - {category_name}"

	doc = frappe.get_doc({
		"doctype": "Bank Transaction Category",
		"category": category_name,
		"parent_bank_transaction_category": parent_category,
		"is_group": parent is None,
		"bridge_category_id": category.get("id")
	})

	return doc.insert(ignore_permissions=True, ignore_if_duplicate=True)

def sync_transactions(bank_transactions):
	categories = {c.bridge_category_id: c.name for c in frappe.get_all("Bank Transaction Category", fields=["bridge_category_id", "name"])}
	accounts = {a.bridge_account_id: (a.name, a.company, getdate(a.bridge_initial_date))
		for a in frappe.get_all("Bank Account", fields=["bridge_account_id", "name", "company", "bridge_initial_date"])}
	for bank_transaction in bank_transactions:
		if cstr(bank_transaction.get("account_id")) in accounts.keys():
			create_update_bank_transaction(bank_transaction, accounts, categories)

def create_update_bank_transaction(bank_transaction, accounts, categories):
	if getdate(bank_transaction.get("date")) < accounts.get(cstr(bank_transaction.get("account_id")))[2]:
		return

	if bank_transaction.get("is_deleted"):
		delete_bank_transaction(bank_transaction)
	elif frappe.db.exists("Bank Transaction", dict(reference_number=bank_transaction.get("id"), docstatus=1)):
		update_bank_transaction(bank_transaction, accounts, categories)
	else:
		create_bank_transaction(bank_transaction, accounts, categories)

def delete_bank_transaction(bank_transaction):
	transaction = frappe.db.get_value("Bank Transaction", dict(reference_number=bank_transaction.get("id"), docstatus=("!=", 2), status="Unreconciled"))
	if transaction:
		doc = frappe.get_doc("Bank Transaction", transaction)
		doc.flags.ignore_permissions = True
		if doc.docstatus == 1:
			return doc.cancel()
		elif doc.docstatus == 0:
			return frappe.delete_doc("Bank Transaction", transaction, ignore_permissions=True)

def update_bank_transaction(bank_transaction, accounts, categories):
	doc = frappe.get_doc("Bank Transaction", dict(reference_number=bank_transaction.get("id")))
	to_delete = False
	for field, value in {
		"date": bank_transaction.get("date"),
		"company": accounts.get(cstr(bank_transaction.get("account_id")))[1],
		"bank_account": accounts.get(cstr(bank_transaction.get("account_id")))[0],
		"currency": bank_transaction.get("currency_code"),
		"debit": abs(flt(bank_transaction.get("amount"))) if flt(bank_transaction.get("amount")) < 0 else 0.0,
		"credit": abs(flt(bank_transaction.get("amount"))) if flt(bank_transaction.get("amount")) > 0 else 0.0
	}.items():
		if cstr(doc.get(field)) != cstr(value):
			if doc.docstatus == 0:
				doc.update({field: value})
			elif doc.status == "Unreconciled":
				to_delete = True
				break

	else:
		for field, value in {
			"category": categories.get(bank_transaction.get("category_id")),
			"description": f'{bank_transaction.get("clean_description")}<br>{bank_transaction.get("bank_description")}'
		}.items():
			if doc.get(field) != value:
				frappe.db.set_value("Bank Transaction", doc.name, field, value)

	if to_delete:
		delete_bank_transaction(bank_transaction)
		create_bank_transaction(bank_transaction, accounts, categories)

def create_bank_transaction(bank_transaction, accounts, categories):
	if (not bank_transaction.get("is_future") and not
		frappe.db.exists("Bank Transaction",
			dict(
				bank_account=accounts.get(cstr(bank_transaction.get("account_id")))[0],
				reference_number=bank_transaction.get("id"),
				docstatus=1
			)
		)
	):
		doc = frappe.get_doc({
			"doctype": "Bank Transaction",
			"date": bank_transaction.get("date"),
			"company": accounts.get(cstr(bank_transaction.get("account_id")))[1],
			"reference_number": bank_transaction.get("id"),
			"bank_account": accounts.get(cstr(bank_transaction.get("account_id")))[0],
			"category": categories.get(bank_transaction.get("category_id")),
			"currency": bank_transaction.get("currency_code"),
			"debit": abs(flt(bank_transaction.get("amount"))) if flt(bank_transaction.get("amount")) < 0 else 0.0,
			"credit": abs(flt(bank_transaction.get("amount"))) if flt(bank_transaction.get("amount")) > 0 else 0.0,
			"description": f'{bank_transaction.get("clean_description")}<br>{bank_transaction.get("bank_description")}'
		})
		doc.insert(ignore_permissions=True, ignore_if_duplicate=True)
		return doc.submit()

def check_onboarding_status(bank):
	from bank.api.bridge.status import BRIDGE_STATUS_MAP

	if "bridge_account_id" not in bank:
		check_if_custom_fields_exists()

	onboarding_steps = {
		"label": _("Configure the synchronization with your bank"),
		"subtitle": _("Retrieve all your bank transactions automatically"),
		"steps": [],
		"widget_type": "form_onboarding",
		"options": {
			"allow_sorting": False,
			"allow_create": False,
			"allow_delete": False,
			"allow_hiding": False,
			"allow_edit": False,
			"max_widget_count": 1
		}
	}

	# 1. Check if item is fully configured
	onboarding_steps["steps"].append({
		"title": _("Establish a connection with your bank"),
		"action": "Trigger Event",
		"action_label": _("Connect your bank"),
		"event": "add_new_bank",
		"is_single": 1,
		"is_pending": not bank.get("bridge_item_id"),
		"is_complete": bank.get("bridge_item_id"),
		"is_active": not bank.get("bridge_item_id")
	})

	# 2. Check if item configuration is finished
	if bank.get("bridge_item_id") and bank.get("bridge_status"):
		onboarding_steps["steps"].append(BRIDGE_STATUS_MAP.get(bank.get("bridge_status")))

	# 3. Check if all accounts are linked
	elif bank.get("bridge_item_id") and not bank.get("bridge_status"):
		bank_accounts = frappe.get_all("Bank Account", filters=dict(bank=bank.name), pluck="bridge_account_id")
		if not bank.get("bridge_status") and (not bank_accounts or not any(bank_accounts)):
			onboarding_steps["steps"].append({
				"title": _("Synchronize all your bank accounts"),
				"action": "Trigger Event",
				"action_label": _("Get your accounts"),
				"event": "get_bank_accounts",
				"is_single": 1,
				"is_active": 1
			})
		else:
			onboarding_steps["steps"] = []

	return onboarding_steps


@frappe.whitelist()
def get_bank_status(bridge_item_id):
	params = {'item_id': bridge_item_id}
	connection = dokos_client_connection("/api/method/bank_api.bridge.get_bridge_item_details")
	response = connection.post_request(params)
	return response.get("status_code") if response else 0