
import frappe
from frappe import _
from frappe.frappeclient import FrappeClient, FrappeException

__version__ = "1.0.0"

class DokosClient(FrappeClient):
	def preprocess(self, params):
		params = super().preprocess(params)
		params["site"] = frappe.conf.get("site_name") or frappe.local.site
		params["lang"] = frappe.local.lang or frappe.db.get_default("lang")

		return params

	def post_process(self, response):
		try:
			return super(DokosClient, self).post_process(response)
		except FrappeException as e:
			if "frappe.exceptions.AuthenticationError" in str(e):
				frappe.throw(_("Authentication error: Please check your credentials."))
			raise


def dokos_client_connection(url=None):
	url = f"{frappe.conf.get('bank_connector_url')}{url}"
	return DokosClient(
		url=url,
		api_key=frappe.conf.bank_connector_key,
		api_secret=frappe.conf.bank_connector_secret
	)