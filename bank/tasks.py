import frappe

def daily():
	for bank in frappe.get_all("Bank", filters={"bridge_item_id": ("is", "set")}, fields=["name", "bridge_status"]):
		doc = frappe.get_doc("Bank", bank.name)
		status = doc.run_method("get_bridge_item_details")

		if status and bank.bridge_status != status:
			doc.run_method("create_notification")
			frappe.db.set_value("Bank", bank.name, "bridge_status", status)
